// # Ejercicio JavaScript

// Edita el archivo script.js para crear una función que haga lo siguiente:

// - Generar una contraseña (número entero aleatorio del 0 al 100)
// - Pedir al usuario que introduzca un número dentro de ese rango.
// - Si el número introducido es igual a la contraseña, aparecerá en pantalla un mensaje indicando que ha ganado; 
    // si no, el mensaje indicará si la contraseña en un número mayor o menor al introducido y dará una nueva oportunidad.
// - El usuario tendrá 5 oportunidades de acertar, si no lo consigue, aparecerá un mensaje indicando que ha perdido.

'use strict';

function getRandonNumber(max) {
    return (Math.floor(Math.random() * max));
}


const numOportunities = 5;
const randomNumber = getRandonNumber(100);

let j = numOportunities;


for (let i = 0; i < numOportunities; i++) {
    const userNumber = parseInt(prompt("Introduce un numero del 0 al 100"));

    if (userNumber === randomNumber) {
        alert("Has ganado");
        break;
    } else {
        if (userNumber > randomNumber) {
            alert("El numero es menor");
        } else {
            alert("El numero es mayor");
        }
        alert(`Te quedan ${j-1} intentos`);
        j = j - 1;
    }
}

