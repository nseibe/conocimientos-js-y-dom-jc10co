// # Ejercicio JavaScript 2

// ** Sin modificar el body en el archivo index.html **, utiliza javascript 
// para crear un div en el que se muestre un reloj con la hora actual y que se actualice automáticamente cada segundo.

// El formato del reloj debe ser HH:MM:SS (siempre dos dígitos por cada unidad).

// Si quieres, puedes añadir estilos para que se vea más bonito, pero no se valorará.

'use strict';

const formatNum = (num) => {
    return num < 10 ? '0' + num : num;
};

const interval = setInterval(() => {
    
    const currentDate = new Date();

    const hour = formatNum(currentDate.getHours());
    const minute = formatNum(currentDate.getMinutes());
    const second = formatNum(currentDate.getSeconds());

    const timeClock = document.querySelector('body');

    timeClock.innerHTML =`
        <div>
            ${hour}:${minute}:${second}
        </div>
    `
}, 1000);
