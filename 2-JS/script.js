"use strict";

// # Ejercicio JavaScript 2

// Edita el archivo script.js para crear una función que reciba una array de objetos (equipos y puntos conseguidos) 
// y muestre por consola el ** nombre ** del que más y del que menos puntos hayn conseguido, con sus respectivos ** totales **.

// Encontrarás un array de ejemplo en el propio documento.

// puntuaciones
const puntuaciones = [
  {
    equipo: "Toros Negros",
    puntos: [1, 3, 4, 2, 10, 8],
  },
  {
    equipo: "Amanecer Dorado",
    puntos: [8, 5, 2, 4, 7, 5, 3],
  },
  {
    equipo: "Águilas Plateadas",
    puntos: [5, 8, 3, 2, 5, 3],
  },
  {
    equipo: "Leones Carmesí",
    puntos: [5, 4, 3, 1, 2, 3, 4],
  },
  {
    equipo: "Rosas Azules",
    puntos: [2, 1, 3, 1, 4, 3, 4],
  },
  {
    equipo: "Mantis Verdes",
    puntos: [1, 4, 5, 1, 3],
  },
  {
    equipo: "Ciervos Celestes",
    puntos: [3, 5, 1, 1],
  },
  {
    equipo: "Pavos Reales Coral",
    puntos: [2, 3, 2, 1, 4, 3],
  },
  {
    equipo: "Orcas Moradas",
    puntos: [2, 3, 3, 4],
  },
];

const puntTotales = puntuaciones.map((value, index) => {
  return {
    equipo: value.equipo,
    totalPuntos: puntuaciones[index].puntos.reduce((acc,value) => acc = acc + value),
  }
});

let puntMayor = puntTotales[0].totalPuntos;
let puntMenor = puntTotales[0].totalPuntos;

for (let i = 0; i < puntTotales.length; i++) {
    
    if (puntTotales[i].totalPuntos > puntMayor) {
      puntMayor = puntTotales[i].totalPuntos;
    };

    if (puntTotales[i].totalPuntos < puntMenor) {
      puntMenor = puntTotales[i].totalPuntos;
    };
};

const equipoMayorPunt = puntTotales.filter((value) => value.totalPuntos === puntMayor);
const equipoMenorPunt = puntTotales.filter((value) => value.totalPuntos === puntMenor);

console.log("Mayor puntuacion: ", equipoMayorPunt);
console.log("Menor puntuacion: ", equipoMenorPunt);